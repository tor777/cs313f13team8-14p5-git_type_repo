package eco.ext;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

//import java.awt.Color;
//import java.awt.Graphics;

//import javax.swing.JComponent;
import java.util.*;
import eco.AbstractCell;
import eco.CellView;
import eco.ComponentCellView;

/**
 * Any number and combination of actors are allowed to occupy an instance of this class.
 */
public class Open extends AbstractCell {
    public Paint g=new Paint(10);

  private Canvas component = new Canvas() {

    private static final long serialVersionUID = -8690420760057894819L;

    protected void paintComponent(Paint g) {
      g.setColor(Color.LTGRAY);
      if (! getOccupants().isEmpty()) {
        g.setColor(Color.YELLOW);
      }
      g.setColor(Color.LTGRAY);
      component.drawRect(0, 0, component.getWidth() - 1, component.getHeight() - 1, g);
    }
  };

  private CellView view ;//= new ComponentCellView(component.drawRect(0, 0, component.getWidth() - 1, component.getHeight() - 1, g ));

  public Open() { super(10000); } // very high capacity

  public String toString() { return "."; }

  public CellView getView() { return view; }
}
